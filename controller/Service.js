var request = require('request')
const csvjson = require('csvjson');
const readFile = require('fs').readFile;
const writeFile = require('fs').writeFile;

function Service(req,res){
    request.post('http://localhost:5002/api/v1/Service',{json:{}},function(error,res1,body){
        if (error){
            res.status(500).send({
                result:false,
                message:error
                
            })
        }
        console.log(body);
        
    })
    const csvData = csvjson.toCSV(req.body, {
        headers: 'key'
    });
    writeFile('./test-data.csv', csvData, (err) => {
        if(err) {
            console.log(err); // Do something to handle the error or just throw it
            throw new Error(err);
        }
        console.log('Success!');
    });
    res.setHeader('Content-disposition', 'attachment; filename=testing.csv');
    res.set('Content-Type', 'text/csv');
    // res.sendFile('test-data.csv')
    res.status(200).send(csvData);
}


module.exports ={
    Service
}