import os
from flask import Flask ,request
from flask_restful import Resource,Api,reqparse
import json,time 
from flask_cors import CORS
from controller.Service import Service
app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})
api = Api(app)

api.add_resource(Service,'/api/v1/Service')

if __name__ == '__main__':
    app.run(debug=True,host="0.0.0.0",port= os.environ['POST'] if 'POST' in os.environ else 5002)
    print("server start ")