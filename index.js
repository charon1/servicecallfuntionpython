var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var routes = require('./routes')
var app = express();
const cors = require('cors');
app.use(cors());
app.use(express.static('public'));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
// app.use(logger(function (tokens, req, res) {
//   console.log({
//     method:tokens.method(req, res),
//     url:tokens.url(req, res),
//     status:tokens.status(req, res),
//     res:tokens.res(req, res, 'content-length'),
//     "response-time":tokens['response-time'](req, res)
//   });
  
// }))
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

routes(app)
// app.use('/api/v1', indexRouter);
// app.use('/users', usersRouter);

// catch 404 and forward to error handler


// error handler

var PORT = process.env.PORT || 5001

app.listen(PORT,console.log('Service Auth Start On Port', PORT))

module.exports = app;
